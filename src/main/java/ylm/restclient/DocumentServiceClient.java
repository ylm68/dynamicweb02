package ylm.restclient;

import org.glassfish.jersey.jackson.JacksonFeature;
import org.glassfish.jersey.media.multipart.FormDataBodyPart;
import org.glassfish.jersey.media.multipart.FormDataMultiPart;
import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.glassfish.jersey.media.multipart.file.FileDataBodyPart;
import org.glassfish.jersey.media.multipart.file.StreamDataBodyPart;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.client.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.net.URI;

/**
 * Created by pleroux on 24/09/2014.
 */
public class DocumentServiceClient {

  public static String BaseURL = "http://localhost:8080/rest-server/rest/";

  private static final Logger LOGGER = LoggerFactory.getLogger(DocumentServiceClient.class);

  private static URI getBaseURI() {
    return UriBuilder.fromUri(BaseURL).build();
  }

  /**
   *
   * @param nomDocumentSurClient
   * @param repertoireSurServeur
   * @param nomDocumentSurClientObligatoire
   * @param repertoireRacineSurServeurSelonTypeDocument
   * @param filtres
   * @param tailleContenuMaxEnKiloOctets
   * @return
   * @throws java.io.FileNotFoundException
   */
  public static Response uploadDocument(String nomDocumentSurClient, String repertoireSurServeur,
      String nomDocumentSurClientObligatoire, String repertoireRacineSurServeurSelonTypeDocument, String filtres,
      Integer tailleContenuMaxEnKiloOctets) throws FileNotFoundException {

    Client client = ClientBuilder.newBuilder().register(MultiPartFeature.class).register(JacksonFeature.class).build();
    WebTarget webtarget = client.target(getBaseURI()).path("document").path("divers");

    File fileToUpload = new File(nomDocumentSurClient);
    if (!fileToUpload.exists()) {
      throw new FileNotFoundException(nomDocumentSurClient + " not found");
    }

    FormDataMultiPart multiPart = new FormDataMultiPart();
    multiPart.setMediaType(MediaType.MULTIPART_FORM_DATA_TYPE);
    if (fileToUpload != null) {
      multiPart.bodyPart(new FileDataBodyPart("document", fileToUpload, MediaType.APPLICATION_OCTET_STREAM_TYPE));
      multiPart.bodyPart(new FormDataBodyPart("repertoireSurServeur", repertoireSurServeur));
      if (nomDocumentSurClientObligatoire != null) {
        multiPart.bodyPart(new FormDataBodyPart("nomDocumentSurClientObligatoire", nomDocumentSurClientObligatoire));
      }
      multiPart.bodyPart(new FormDataBodyPart(
          "repertoireRacineSurServeurSelonTypeDocument",
          repertoireRacineSurServeurSelonTypeDocument));
      multiPart.bodyPart(new FormDataBodyPart("filtres", filtres));
      multiPart.bodyPart(new FormDataBodyPart("tailleContenuMaxEnKiloOctets", tailleContenuMaxEnKiloOctets.toString()));
    }

    Invocation.Builder invocationBuilder = webtarget.request();
    Response response = invocationBuilder.post(Entity.entity(multiPart, multiPart.getMediaType()));

    LOGGER.debug(response.getStatus() + " " + response.getStatusInfo() + " " + response);
    return response;
  }

  /**
   *
   * @param nomDocument
   * @param repertoireSurServeur
   * @return
   */
  public static Response downloadDocumentContent(String nomDocument, String repertoireSurServeur) {

    LOGGER.trace("downloadDocumentContent(" + nomDocument + ") dans répertoire serveur : " + repertoireSurServeur);
    Client client = ClientBuilder.newBuilder().register(MultiPartFeature.class).build();
    WebTarget webtarget =
        client.target(getBaseURI()).path("document").path("divers").queryParam("nomDocument", nomDocument).queryParam(
            "repertoire",
            repertoireSurServeur);
    Invocation.Builder invocationBuilder = webtarget.request();
    Response response = invocationBuilder.get(Response.class);

    LOGGER.debug(response.getStatus() + " " + response.getStatusInfo() + " " + response);
    return response;
  }

  /**
   *
   * @param nomDocumentSurClient
   * @param repertoireSurServeur
   * @param nomDocumentSurClientObligatoire
   * @return
   * @throws java.io.FileNotFoundException
   */
  public static Response uploadCV(String nomDocumentSurClient, String repertoireSurServeur,
      String nomDocumentSurClientObligatoire) throws FileNotFoundException {

    Client client = ClientBuilder.newBuilder().register(MultiPartFeature.class).build();
    WebTarget webtarget = client.target(getBaseURI()).path("document").path("cv");

    File fileToUpload = new File(nomDocumentSurClient);
    if (!fileToUpload.exists()) {
      throw new FileNotFoundException(nomDocumentSurClient + " not found");
    }

    FormDataMultiPart multiPart = new FormDataMultiPart();
    multiPart.setMediaType(MediaType.MULTIPART_FORM_DATA_TYPE);
    if (fileToUpload != null) {
      multiPart.bodyPart(new FileDataBodyPart("document", fileToUpload, MediaType.APPLICATION_OCTET_STREAM_TYPE));
      multiPart.bodyPart(new FormDataBodyPart("repertoireSurServeur", repertoireSurServeur));
      if (nomDocumentSurClientObligatoire != null) {
        multiPart.bodyPart(new FormDataBodyPart("nomDocumentSurClientObligatoire", nomDocumentSurClientObligatoire));
      }
    }

    Invocation.Builder invocationBuilder = webtarget.request();
    Response response = invocationBuilder.post(Entity.entity(multiPart, multiPart.getMediaType()));

    LOGGER.debug(response.getStatus() + " " + response.getStatusInfo() + " " + response);
    return response;
  }

  /**
   *
   * @param nomDocumentSurClient
   * @param repertoireSurServeur
   * @param nomDocumentSurClientObligatoire
   * @return
   * @throws java.io.FileNotFoundException
   */
  public static Response uploadStreamCV(InputStream fileInputStream, String nomDocumentSurClient,
      String repertoireSurServeur, String nomDocumentSurClientObligatoire) {

    Client client = ClientBuilder.newBuilder().register(MultiPartFeature.class).build();
    WebTarget webtarget = client.target(getBaseURI()).path("document").path("cv");

    FormDataMultiPart multiPart = new FormDataMultiPart();
    multiPart.setMediaType(MediaType.MULTIPART_FORM_DATA_TYPE);
    if (fileInputStream != null) {
      multiPart.bodyPart(new StreamDataBodyPart(
          "document",
          fileInputStream,
          nomDocumentSurClient,
          MediaType.APPLICATION_OCTET_STREAM_TYPE));
      multiPart.bodyPart(new FormDataBodyPart("repertoireSurServeur", repertoireSurServeur));
      if (nomDocumentSurClientObligatoire != null) {
        multiPart.bodyPart(new FormDataBodyPart("nomDocumentSurClientObligatoire", nomDocumentSurClientObligatoire));
      }
    }

    Invocation.Builder invocationBuilder = webtarget.request();
    Response response = invocationBuilder.post(Entity.entity(multiPart, multiPart.getMediaType()));

    LOGGER.debug(response.getStatus() + " " + response.getStatusInfo() + " " + response);
    return response;
  }

  /**
   *
   * @param nomDocumentSurClient
   * @param repertoireSurServeur
   * @param nomDocumentSurClientObligatoire
   * @return
   * @throws java.io.FileNotFoundException
   */
  public static Response uploadCVTemporaire(String nomDocumentSurClient, String repertoireSurServeur,
      String nomDocumentSurClientObligatoire) throws FileNotFoundException {

    Client client = ClientBuilder.newBuilder().register(MultiPartFeature.class).build();
    WebTarget webtarget = client.target(getBaseURI()).path("document").path("cv").path("temporaire");

    File fileToUpload = new File(nomDocumentSurClient);
    if (!fileToUpload.exists()) {
      throw new FileNotFoundException(nomDocumentSurClient + " not found");
    }

    FormDataMultiPart multiPart = new FormDataMultiPart();
    multiPart.setMediaType(MediaType.MULTIPART_FORM_DATA_TYPE);
    if (fileToUpload != null) {
      multiPart.bodyPart(new FileDataBodyPart("document", fileToUpload, MediaType.APPLICATION_OCTET_STREAM_TYPE));
      multiPart.bodyPart(new FormDataBodyPart("repertoireSurServeur", repertoireSurServeur));
      if (nomDocumentSurClientObligatoire != null) {
        multiPart.bodyPart(new FormDataBodyPart("nomDocumentSurClientObligatoire", nomDocumentSurClientObligatoire));
      }
    }

    Invocation.Builder invocationBuilder = webtarget.request();
    Response response = invocationBuilder.post(Entity.entity(multiPart, multiPart.getMediaType()));

    LOGGER.debug(response.getStatus() + " " + response.getStatusInfo() + " " + response);
    return response;
  }

  /**
   *
   * @param nomDocument
   * @return
   */
  public static Response downloadCVContent(String nomDocument) {

    LOGGER.trace("downloadCVContent(" + nomDocument + ")");
    Client client = ClientBuilder.newBuilder().register(MultiPartFeature.class).build();
    WebTarget webtarget =
        client.target(getBaseURI()).path("document").path("cv").queryParam("nomDocument", nomDocument);
    Invocation.Builder invocationBuilder = webtarget.request();
    Response response = invocationBuilder.get(Response.class);

    LOGGER.debug(response.getStatus() + " " + response.getStatusInfo() + " " + response);
    return response;
  }

}
