package ylm.servlet;

import org.apache.commons.lang.RandomStringUtils;
import ylm.restclient.DocumentServiceClient;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import javax.ws.rs.core.Response;
import java.io.IOException;

/**
 * Created by pleroux on 23/10/2014.
 */

@WebServlet("/UploadServlet")
@MultipartConfig(fileSizeThreshold = 1024 * 1024 * 2, maxFileSize = 1024 * 1024 * 10, maxRequestSize = 1024 * 1024 * 50)
public class UploadDocumentServlet extends HttpServlet {

  @Override
  protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    String titreDocument = request.getParameter("titreDocument");
    Part part = request.getPart("document");
    String fileName = extractFileName(part);

    RandomStringUtils random = new RandomStringUtils();

    Response uploadCVResponse =
        DocumentServiceClient.uploadStreamCV(part.getInputStream(), fileName, random.randomAlphabetic(20), null);

    request.setAttribute("titreDocument", titreDocument);
    request.setAttribute("nomFichier", fileName);
    request.setAttribute("message", uploadCVResponse.getStatusInfo());
    getServletContext().getRequestDispatcher("/out.jsp").forward(request, response);
  }

  protected String extractFileName(Part part) {
    String contentDisp = part.getHeader("content-disposition");
    String[] items = contentDisp.split(";");
    for (String s : items) {
      if (s.trim().startsWith("filename")) {
        return s.substring(s.indexOf("=") + 2, s.length() - 1);
      }
    }
    return "";
  }

}
