package ylm.servlet;

import ylm.restclient.DocumentServiceClient;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.Response;
import java.io.IOException;

/**
 * Created by pleroux on 27/10/2014.
 */
@WebServlet("/DownloadServlet")
public class DownloadDocumentServlet extends HttpServlet {

  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    String nomDocument = request.getParameter("nomDocument");
    String repertoireSurServeur = request.getParameter("repertoire");

    Response uploadCVResponse = DocumentServiceClient.downloadDocumentContent(nomDocument, repertoireSurServeur);

    getServletContext().getRequestDispatcher("/out.jsp").forward(request, response);
  }

}
